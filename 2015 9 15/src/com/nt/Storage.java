package com.nt;

public class Storage {
	private int r;
	private boolean isEmpty=true;
	synchronized void put(int i){
		while(! isEmpty){
			try{
				this.wait();
			}catch(InterruptedException E){System.out.println(E.getMessage());}
		}
		r=i;
		isEmpty=false;
		notify();
	}
	synchronized int get(){
		while(! isEmpty){
			try{
				this.wait();
			}catch(InterruptedException E){System.out.println(E.getMessage());}
	}
		isEmpty=true;
		notify();
		return r;
	}
}
	