package com.nt;

public class Demo {
	public static void main(String[] args) {
		Storage s = new Storage();
		Generate g = new Generate(s);
		Receive r = new Receive(s);
		g.start();
		r.start(); 
	}
}
