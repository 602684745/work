package com.nt;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class socket {
	private static DatagramSocket server;
	public static void main(String args[]) throws IOException{
		server = new DatagramSocket(4000);
		byte[] buf=new byte[256];
		DatagramPacket packet = new DatagramPacket(buf,buf.length);
		while(true){
			server.receive(packet);
			String text = new String(packet.getData());
			System.out.println(packet.getAddress().getHostAddress()+":"+packet.getPort()+"=-="+text);
			if(text.startsWith("bye")){
				break;
			}
		}
		server.close();
	}
	
}